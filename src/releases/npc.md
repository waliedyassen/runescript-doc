# Npc

This page documents all known `npc` related releases.

## npc-1

[Original](https://www.reddit.com/r/2007scape/comments/6qifsv/our_rendition_of_an_abyssal_demon) - [Mirror](./mirrors/npc-1.png)

> Title: Our Rendition of an Abyssal Demon

```
[slayer_abyssal]
name-Abyssal demon
desc=A denizen of the Abyss!
size=l
model1=skill_slayer_npc_abyssal
walkanim=abyssal_walk
readyanim=abyssal_ready
param=death_anim,abyssal_death
param=damagetype,0
param=Attack_anim,abyssal_attack
param=defend_anim,abyssal_demon_teleport
recol1s=23808
recol1d=6210
resizeh=64
resizev=64
attack=97
strength=67
hitpoints=150
defence=135
param=slashdefence,20
param=stabdefence,20
param-rangedefence,20
param=crushdefence,20
op2=Attack
respawnrate=30
param=demonbane_vulnerable,^true
param=slayer_category,^slayer_target_abyssaldemons
param=attack_sound,abyssal_attack
param=defend_sound,abyssal_hit
param=death_sound,abyssal_death
category=slayer_abyssaldemon
param=slayer_level,85
param=death_drop,ashes
param=superior_mob,superior_abyssal_demon
param=superior_size,1
```

## npc-2

[Original](https://discord.com/channels/177206626514632704/269673599554551808/870042881631604796) - [Mirror](./mirrors/npc-2.png)

> Spine: NOW, let's add in the ~~araxyte~~ spider drop table<br>
> Ed: What's an araxyte?<br>
> Spine: oh, you<br>
> Spine: :meeeeow:<br>
> Ed: Well would you look at that {image}

```
[araxyte_large]
name=Spider
desc=A very large odd-looking spider.
size=2
category=araxyte
model1=npc_spider_araxyte01
resizeh=68
resizev=68
readyanim=npc_araxyte01_idle
walkanim=npc_araxyte01_walk
param=attack_anim,npc_araxyte01_attack
param=defent_anim,null
param=death_anim,npc_araxyte01_death
param=deathanim_delay,2
param=attack_sound,insect_attack
param=defend_sound,insect_hit
param=death_sound,insect_death
wanderrange=6
maxrange=10
param=slayer_category,^slayer_target_spiders
op2=Attack
huntrange=5
attack=120
strength=120
defence=80
magic=80
hitpoints=100
param=attackrate,4
param=stabdefence,60
param=slashdefence,60
param=crushdefence,20
param=rangedefence,180
param=magicdefence,20
param=death_drop,null
```

## npc-3

[Original](https://twitter.com/JagexStu/status/790930007891861505) - [Mirror](./mirrors/npc-3.jpg)

> @JagexStu: Updating White Knight armour. So far: QBD cave (15), Recruit. Drive (8), Pest Queen incl domtower (3),
> Death Plateau (1), RotM (1) Missing?

> @JagexStu: In a world where White Knights are truly well-ordered...they don't have config names like
> [rd_teleporter_guy].

```
[rd_teleporter_guy]
name=Sir Tiffy Cashien
desc=Head of recruitment for the Temple Knights.
model1=man_head_balding
model2=man_jaw_long
model3-man_headextra_monacle2
model4=2011_white_armour_male_full_boots
mode15=2011_white_armour_male_full_hands
model6=2011_white_armour_male_full_legs_t3
model7=2011_white_armour_male_full_torso_t3
model8=falsiege_tiffy_tea
model9=human_weapons_bench
head1=chat_male_head_balding_youngjaw
head2=chat_male_jaw_long
head3-chat_male_headextra_monacle
bas=tomeraider_tiffy
vislevel=0
size=1
```

## npc-4

[Original](https://twitter.com/JagexOrion/status/1181969339446153217) - [Mirror](./mirrors/npc-4.png)

> @JagexOrion: One of our animators has clearly had a stroke

```
Reason: Unrecognised SEQ: pof_dino_lizard_baby_walky_walk
Asset: dino_lizard_baby_baby
Line: 129
>> walkforwards=pof_dino_lizard_baby_walky_walk
```

## npc-5

[Original](https://discord.com/channels/177206626514632704/424846377621651456/782366948968103966) - [Mirror 1](./mirrors/npc-5.0.png) - [Mirror 2](./mirrors/npc-5.1.png)

> gaucho: [npc-5-0](#npc-5-0)<br>
> guacho: Hunting other npcs sounds like custom GWD or later behaviour<br>
> guacho: So it might not be in the list<br>
> Deleted User: dorgeshkaan goblins do it<br>
> Deleted User: and they can even set theirself as target<br>
> gaucho: Arceuus reanimated creatures: [npc-5-1](#npc-5-1)

### npc-5-0

```
gosub(battle_tortoise_npc_coord_magic);
.npc_add(%tempcoord,gnome_mage,80);
.npc_sethuntmode(aggressive_ranged);
.npc_sethunt(20);
gosub(battle_tortoise_npc_coord_rider);
.npc_add(%tempcoord,gnome_driver,80);
.npc_setmode(opplayer2);
```

### npc-5-1

```
category=arceuus_reanimated
param=foe_only,^true
param=death_drop,null
maxrange=20
huntrange=10
huntmode=aggressive_melee
members=yes
```

## npc-6

[Original](https://twitter.com/JagexAsh/status/519449302138363904) - [Mirror 1](./mirrors/npc-6.png)

> @JagexAsh: For my 9000th tweet, here's an extract from an NPC definition in the @OldSchoolRS codebase. Is the guy
> Malak or not?

```
[Malak]
//actually not malak
size=1
name=Ruantun
desc=Luckily, I can't see much of his face.
members=yes
```

## npc-7

[Original](https://twitter.com/ZenKris21/status/1603802392658690064) - [Mirror](./mirrors/npc-7.png)

> @ZenKris21: @JagexAsh How do you define the coords for NPCs which patrol, such as Hans and Strange old man? Is it some
> kind of a config?

> @JagexAsh: Yes, in the form of a list of coordinates.

> @ZenKris21: Is there anything else that's configurable for patrols? I've noticed that you can only block the NPC for
> so long until it teleports to the next checkpoint, but that delay appears to be different per each NPC?<br>Strange old
> man also appears to wait at each checkpoint 🧐

> @JagexAsh: Ah - it's a list of coordinates and a number of ticks to pause at each one.<br>From trying things in-game,
> I got the impression that the NPC would teleport past obstacles after whatever time it was due to have reached its
> next
> patrol point and finished the pause there.

> @ZenKris21: Ah, that idea did cross my mind.<br><br>Are these coordinates stored in some unique config, or would they
> be part of the NPC's own config?

> @JagexAsh: Part of the NPC's own config.

> @ZenKris21: Any chance you could show a small snippet of how these coordinates & number of ticks are defined in the
> config? Primarily interested in the syntax and naming 😁

> @JagexAsh: P.S. Note that his behaviour mode is specified as `patrol`, so that's what he reverts to doing when he
> resets at the end of dialogue, etc.

> @JagexAsh: Hans:
> ```
> defaultmode=patrol
> patrol1=0_50_50_7_33,0
> patrol2=0_50_50_11_30,0
> patrol3=0_50_50_19_30,0
> patrol4=0_50_50_19_22,10
> patrol5=0_50_50_21_22,0
> patrol6=0_50_50_21_12,0
> patrol7=0_50_50_18_9,0
> patrol8=0_50_50_14_5,0
> patrol9=0_50_50_2_5,0
> patrol10=0_50_50_2_32,0
> ```

## npc-8

[Original](https://twitter.com/ZenKris21/status/1604502908971601921)

> @ZenKris21: @JagexAsh Is there anything that can be customized regarding `playerfollow` or `playerescape` modes? Like
> how with patrolling, there's the pausing at each coordinate.

> @JagexAsh: Not specifically, though the NPC's `maxrange` setting (part of the NPC's own config) would determine how
> far it can go while following you or fleeing you.

## npc-9

> @ZenKris21: @JagexAsh Is the mechanic where NPCs teleport to their spawn coord after 5 minutes of not moving done in
> the engine? Does this mechanic only get triggered in wandering mode?

> @JagexAsh: It's in the engine, yes. I can't see what's under the hood there, but I don't remember seeing an NPC do it
> except when in wandering mode.

## npc-10

[Original](https://twitter.com/ZenKris21/status/1605260518087860227)

> @ZenKris21: @JagexAsh Does a NPC version of p_aprange exist?

> @JagexAsh: No, they have an `attackrange` specified in their config instead, which has a similar effect.

> @ZenKris21: Interesting. Does this `attackrange` have any sort of limitations? aprange appears to be capped to 10,
> however I think I’ve seen NPCs attack from as far as 15.

> @JagexAsh: I've seen values of 40 used. Not sure what the limit is.

> @JagexAsh: (P.S. Fragment of Seren is an example of what I just listed.)

> @TheCrazy0neTv: For clarity, is `attackrange` a param or an actual property of npcs?

> @JagexAsh: A property, not just something scripted as a param.<br>I think it could well have been done as a param, in
> hindsight, but there's little incentive to rewrite it as 'content' rather than 'engine' at present.

> @TheCrazy0neTv: Interesting, so from your knowledge the engine does nothing with it? Does that also mean it's exposed
> to RuneScript via `nc_attackrange` or something similar?

> @JagexAsh: The engine uses it alright, bringing NPCs closer to their target if they're trying to interact from a
> distance. My point was that this could have been scripted behaviour, like it is for players for whom it's scripted
> based on their current weapon.<br>`npc_attackrange()` returns it.

## npc-11

[Original](https://twitter.com/ZenKris21/status/1605260518087860227)

> @ZenKris21: @JagexAsh Do you have the ability to define the distance at which an interaction stops from the engine's
> perspective? E.g. NPCs appear to stop interacting with you the moment you go more than one tile away from them, while
> in a dialogue with them. If not, how would that be done?

> @JagexAsh: Are you aware of the `maxrange` setting that NPCs can have? That determines the distance for most
> interactions, e.g. combat aggression.<br>One special case is the mode used by standard dialogue, though, which always
> resets when they stop being adjacent.

> @ZenKris21: Right, that special case was what I was referring to. Do you have any control over it? NPCs like the
> Zanaris bankers only break the dialogue interaction when going further than 3 tiles from them, rather than just
> adjacent.

> @JagexAsh: The engine has that one special mode for 'reset when no longer adjacent'.<br>Banker dialogue tends not to
> use that mode, as most bankers aren't adjacent. (Even if the Zanaris ones actually are.) So those would more likely
> depend on the NPC's `maxrange` setting I mentioned.

> @ZenKris21: Is the mode you speak of a special npc_mode? What's the internal name behind it?

> @JagexAsh: Yes - `playerfaceclose`.<br>Bankers commonly use a different one, called `playerface`, which - ahem - makes
> them face the player.

> @TheCrazy0neTv: What causes the npc to "target" the person that talked with them? Is it done automatically or do you
> need to do like npc_targetplayer(uid) or something similar?

> @JagexAsh: It's a command like you guessed - npc_setmode(playerfaceclose), or equivalent.

## npc-12

[Original](https://twitter.com/JagexAsh/status/1549466743273316353)

> @helloiamvenn: @JagexAsh Hi just curious, how is the position that sheep flee to determined?

> @JagexAsh: Is this in Sheep Herder or just for shearing the normal ones?

> @helloiamvenn: Just for the normal sheep.

> @JagexAsh: The engine's got a predefined behaviour mode called `playerescape` that makes the NPC move away from the
> specified player. Usually in a straight line, until it reaches obstacles.

## npc-13

[Original](https://twitter.com/Wotury/status/1603855636604649473)

> @Wotury: @JagexAsh Do the Draynor Manor chairs have a wander range specified in their config?

> @JagexAsh: Specified as zero.

> @Wotury: Interesting. What about the boulder that blocks the entrance to the eagle transport system in the desert? I
> suppose it's wander range is 1? If so, what prevents it from wandering around?

> @JagexAsh: It's got a movement restriction telling it to never move, rather than that, though the effect is pretty
> similar.

> @Wotury: Is it a property in it's config? I'm curious about the name of such property and the values it can take since
> I've never heard of it before.

> @JagexAsh: `moverestrict` can be `normal`, `blocked` (e.g. ducks walk on water), `blocked+normal` (e.g. implings walk
> over land and water), `indoors`, `outdoors` or `nomove`.

> @Wotury: Thank you! So I'm guessing the Draynor manor chairs have `moverestrict` set to `nomove`?

> @JagexAsh: `indoors`, actually. `nomove` means the thing doesn't move unless some code manually teleports it.
