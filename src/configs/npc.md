# Npc Documentation

Example npc config. See [releases/npc](../releases/npc.md) for more examples.

```
[araxyte_large]
name=Spider
desc=A very large odd-looking spider.
size=2
category=araxyte
model1=npc_spider_araxyte01
resizeh=68
resizev=68
readyanim=npc_araxyte01_idle
walkanim=npc_araxyte01_walk
param=attack_anim,npc_araxyte01_attack
param=defent_anim,null
param=death_anim,npc_araxyte01_death
param=deathanim_delay,2
param=attack_sound,insect_attack
param=defend_sound,insect_hit
param=death_sound,insect_death
wanderrange=6
maxrange=10
param=slayer_category,^slayer_target_spiders
op2=Attack
huntrange=5
attack=120
strength=120
defence=80
magic=80
hitpoints=100
param=attackrate,4
param=stabdefence,60
param=slashdefence,60
param=crushdefence,20
param=rangedefence,180
param=magicdefence,20
param=death_drop,null
```

## Properties

The follow table lists all known properties used in an npc config. Properties marked with an asterisk (`*`) are server
only.

| Name                 | Description                                                                  |
|----------------------|------------------------------------------------------------------------------|
| `name`               | The name as seen in game.                                                    |
| `desc`               | The description of the npc, commonly referred to as its examine.             |
| `size`               | The size of the npc in terms of tiles. A `size` of `2` means the npc is 2x2. |
| `readyanim`          | The animation used when the npc is sitting idle.                             |
| `walkanim`           | The animation used when the npc is walking.                                  |
| `category`           | <!-- TODO: link to category docs -->                                         |
| `head1`..`head5`     | Defines a head model to use.                                                 |
| `model1`..`model12`  | Defines a model to use.                                                      |
| `resizeh`            | Horizontal width of the model.                                               |
| `resizev`            | Vertical height of the model.                                                |
| `recol1s`..`recol6s` | Defines a color to find on the original model.                               |
| `recol1d`..`recol6d` | Defines a color that replaced the one found using `recol1s`.                 |
| `ambient`            |                                                                              |
| `contrast`           |                                                                              |
| `op1`..`op5`         | Defines an op at the given index.                                            |
| `maxrange`*          |                                                                              |
| `moverestrict`*      | Defines movement behavior.                                                   |
| `wanderrange`*       | The maximum distance from the npc spawn point it will wander around.         |
| `attackrange`*       |                                                                              |
| `huntrange`*         | The maximum distance the npc will hunt for targets.                          |
| `huntmode`*          | The default hunt type to use.                                                |
| `attack`*            | Defines the attack skill level.                                              |
| `strength`*          | Defines the strength skill level.                                            |
| `defence`*           | Defines the defence skill level.                                             |
| `ranged`*            | Defines the ranged skill level.                                              |
| `magic`*             | Defines the magic skill level.                                               |
| `hitpoints`*         | Defines the hitpoints skill level.                                           |
| `vislevel`           | Overrides the calculated combat level. A value of `0` means no level shown.  |

## Modes

An `npc_mode` is a value that is given to an npc that changes its behavior. The following table lists all known modes
a brief description. Click the mode name for more information when applicable.

| Name                                      | Description                                                         |
|-------------------------------------------|---------------------------------------------------------------------|
| [`wander`](#wander)                       | The default mode. Makes the npc to wander around their spawn point. |
| [`patrol`](#patrol)                       | Makes the npc to follow a specific path.                            |
| `playerescape`                            | Makes the npc attempt to retreat from its target.                   |
| `playerfollow`                            | Makes the npc follow its target.                                    |
| `playerface`                              | Makes the npc face its target while within the npcs `maxrange`.     |
| `playerfaceclose`                         | Makes the npc face its target while within `1` square of the npc.   |
| [`opplayer1`..`opplayer8`](#interactions) | Executes `[ai_opplayer1,{npc}]` script every game cycle.            |
| [`applayer1`..`applayer8`](#interactions) | Executes `[ai_applayer1,{npc}]` script every game cycle.            |
| [`opnpc1`..`opnpc5`](#interactions)       | Executes `[ai_opnpc1,{npc}]` script every game cycle.               |
| [`apnpc1`..`apnpc5`](#interactions)       | Executes `[ai_apnpc1,{npc}]` script every game cycle.               |
| [`oploc1`..`oploc5`](#interactions)       | Executes `[ai_oploc1,{npc}]` script every game cycle.               |
| [`aploc1`..`aploc5`](#interactions)       | Executes `[ai_aploc1,{npc}]` script every game cycle.               |
| [`opobj1`..`opobj5`](#interactions)       | Executes `[ai_opobj1,{npc}]` script every game cycle.               |
| [`apobj1`..`apobj5`](#interactions)       | Executes `[ai_apobj1,{npc}]` script every game cycle.               |

### Wander

This is the default mode given to npcs unless specified manually. This mode enables npcs to wander around their spawn
point with a maximum range being defined by `wanderrange`.

```
defaultmode=wander
wanderrange=10
```

### Patrol

This mode allows specifying waypoints and how long the npc will pause at that way point before going to the next one.

The following is a snippet from [npc-7](../releases/npc.md#npc-7).

```
defaultmode=patrol
patrol1=0_50_50_7_33,0
patrol2=0_50_50_11_30,0
patrol3=0_50_50_19_30,0
patrol4=0_50_50_19_22,10
patrol5=0_50_50_21_22,0
patrol6=0_50_50_21_12,0
patrol7=0_50_50_18_9,0
patrol8=0_50_50_14_5,0
patrol9=0_50_50_2_5,0
patrol10=0_50_50_2_32,0
```

### Interactions

## Move restrict

This property allows defining restrictions on how an npc moves.

| Name             | Description                                        | Example        |
|------------------|----------------------------------------------------|----------------|
| `normal`         | Allows movement only on tile that are not blocked. | Man            |
| `blocked`        | Allows movement only on tiles that are blocked.    | Duck           |
| `blocked+normal` | Combination of `normal` and `blocked`.             | Implings       |
| `indoors`        | Allows movement to tiles inside of a building.     | Shop keepers   |
| `outdoors`       | Allows movement to tiles outside of a building.    | Falador Guards |
| `nomove`         | Disallows movement outside of code that teleports. |                |
