# Summary

[Home](./index.md)

---

# Scripts

- [Syntax](./scripts/syntax.md)
- [RuneScript]()
    - [Triggers]()
- [ClientScript]()
    - [Triggers](./scripts/clientscript/triggers.md)
- [Pointers]()
  - [Protected access](./scripts/pointers/protected.md)

---

# Configs

- [area](./configs/area.md)
- [controller]()
- [dbrow]()
- [dbtable]()
- [enum]()
- [headbar]()
- [hitmark]()
- [hunt]()
- [idk]()
- [inv]()
- [loc]()
- [mel]()
- [mesanim]()
- [npc](./configs/npc.md)
- [obj](./configs/obj.md)
- [overlay]()
- [param]()
- [seq]()
- [spotanim]()
- [struct]()
- [underlay]()
- [varbit]()
- [varc]()
- [varclan]()
- [varclansetting]()
- [varcon]()
- [varconbit]()
- [varn]()
- [varnbit]()
- [varobj]()
- [varp]()

---

# Releases

- [script](./releases/script.md)
- [area](./releases/area.md)
- [hunt](./releases/hunt.md)
- [npc](./releases/npc.md)
- [obj](./releases/obj.md)
- [seq](./releases/seq.md)
- [varobj](./releases/varobj.md)
