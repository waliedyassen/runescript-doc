# RuneScript & ClientScript

This page documents all known `rs2` and `cs2` related releases.

# script-1

**Unknown source** - [Image 1](./mirrors/script-1.png)

```clientscript
[opnpc1,Artist1]
if (map_members = 0)
{
  ~chatnpc("<p,angry>Bah! A great artist such as myself should not have to suffer the HUMILIATION of spending time on these dreadful worlds where non-members wander everywhere!");
  return;
}
if (testbit(%BioErrand,6)=1)
{
  jump(DevGotVial);
}
if (%Biohazard=12)
{
  jump(Devpte);
}
mes("Da vinci does not feel sufficiently moved to talk.");

[label,Devpte]
~chatplayer("<p,neutral>Hello, I hear you're an errand boy for the chemist.");
~chatnpc("<p,quiz>Well that's my job yes. But I don't necessarily define my identity in such black and white terms.");
~chatplayer("<p,neutral>Good for you. Now can you take a vial to Varrock for me?");
~chatnpc("<p,shifty>Go on then.");
@multi3("You give him the vial of ethena...",Divinci10,"You give him the vial of liquid honey...",Divinci11,"You give him the vial of sulphuric broline...",Devinci12);

[label,Divinci10]
if (inv_total(inv,Ethenea)<1)
{
  ~mesbox("You can't give him what you don't have.");
  return;
}
inv_del(inv,Ethena,1);
%BioErrand=setbit(%BioErrand,6);
%BioErrand=setbit(%BioErrand,3);
mes("You give him the vial of ethenea.");
~chatplayer("<p,neutral>Ok, we're meeting at the Dancing Donkey in Varrock right?");
~chatnpc("<p,happy>That's right.");
```

# script-2

**Unknown source** - [Image 1](./mirrors/script-2.png)

```clientscript
// Read the clue
[opheld1,trail_hard_riddle_exp1]
~full_trail_readclue("Gold I see, yet gold I require.<br>Give me 875<br>if death you desire.");

// Code for this clue
[proc,trail_hard_riddle_exp1]
// Delete the clue....
inv_del(inv,trail_hard_riddle_exp1, 1);
// ....and give the player a casket!
~objbox(casket,"You've found a casket!");

[opheld1,trail_clue_hard_riddle002_casket]
%namedobj = trail_clue_hard_riddle002_casket;
%s1 = "You've found another clue!";
// Jump to solve clue.
~trail_solvehardclue;

//----------
```

# script-3

[Source 2](https://twitter.com/JagexAsh/status/685175208920104961) - [Image 1](./mirrors/script-3-1.png) - [Image 2](./mirrors/script-3-2.png)

> @SKSC_MosMoForce: @JagexAsh I was wondering how you guys handle skilling with your inhouse language RuneScript saw
> your item drop table one wich was intrestin

> @JagexAsh: @SKSC_MosMoForce I happen to have some smithing code available for
> reading. [Image 2](./mirrors/script-3-2.png)

```clientscript
[if_button1,smithing:arrowheads] @smith_arrowheads(1);
[if_button2,smithing:arrowheads] @smith_arrowheads(5);
[if_button3,smithing:arrowheads] @smith_arrowheads(10);
[if_button4,smithing:arrowheads] @smith_arrowheads(0);
[if_button10,smithing:arrowheads] @smith_arrowheads(null);

[label,smith_arrowheads](int $count)
def_namedobj $product = null;
def_obj $bar = enum(int,obj,brut_ingot_enum,%smithing_bar_type);
if ($bar = bronze_bar) $product = bronze_arrowheads;
else if ($bar = iron_bar) $product = iron_arrowheads; 
else if ($bar = steel_bar) $product = steel_arrowheads; 
else if ($bar = mithril_bar) $product = mithril_arrowheads; 
else if ($bar = adamantite_bar) $product = adamant_arrowheads; 
else if ($bar = runite_bar) $product = rune_arrowheads;
else @general_if_close;
if ($count ! null) @smith_generic($count,$product,$bar);
mes(oc_desc($product));

[label,smith_generic](int $count, namedobj $product, obj $bar)
if ($count <= 0)
{
  ~p_countdialog;
  if (last_int <= 0) return;
  $count = last_int;
}
// Most items cannot be made on Tutorial Island.
if (inzone(0_48_148_0_0,0_48_63_63,coord) = ^true)
{
  if ($product ! bronze_dagger)
  {
    ~mesbox("You cannot make this on Tutorial Island.")
    return;
  }
}
else if (map_members = ^false)
{
  if (oc_members($product) = ^true)
  {
    ~mes("You can only make that on a members' server.");
    return;
  }
}
def_int $level = enum(obj,int,smithing_levelrequired,$product);
def_string $levelfailmessage = enum(obj,string,smithing_levelfailure,$product);
if (stat(smithing) < $level)
{
  ~mesbox($levelfailmessage);
  return;
}
def_int $bar_count = enum(obj,int,smithing_barsrequired,$product);
if (inv_total(inv,$bar) < $bar_count)
{
  ~mesbox("You don't have enough <enum(obj,string,smithing_bar_name_plural,$bar)> to make <enum(obj,string,smithing_products,$product)>");
  return;
}
if_close;
anim(human_smithing,0);
weakqueue*(smithing_generic,3)(coord,map_clock,$count,$product,enum(obj,int,smithing_productquantity,$product),$bar,$bar_count,$level,$levelfailmessage,enum(obj,int,smithing_bar_xp,$bar))
```

# script-4

**Unknown source** - [Image 1](./mirrors/script-4.png)

```clientscript
//Translate-readyt - checked by ############ on the 15th of February 2006
// Edited 5/9/6 by ############, as part of the treasure trails rework

[ai_queue3,_chicken]
gosub(npc_death);
if (npc_findhero=1)
{
  obj_add(npc_coord,npc_param(death_drop),1,200);
  if (map_members = 1) {
    // Treasure trail
    gosub(trail_checkmediumdrop);
  }
  
  // Normal drop
  obj_add(npc_coord,raw_chicken,1,200);
  
  %temp=random(128);
  
  if (%temp
  {
    obj_add
    return(
  }
  
  if (%temp
  {
    obj_add
    return(
  }
}
return();
```

# script-5

**Unknown source** - [Image 1](./mirrors/script-5.png)

```clientscript
[if_button1,bankside:lootingbag_items] @bank_lootingbag_deposit(last_comsubid,1);
[if_button2,bankside:lootingbag_items] @bank_lootingbag_deposit(last_comsubid,5);
[if_button3,bankside:lootingbag_items] @bank_lootingbag_deposit(last_comsubid,^max_32bit_int);
[if_button4,bankside:lootingbag_items] @bank_lootingbag_deposit(last_comsubid,null);

[label,bank_lootingbag_deposit](int $slot, int $requested_number)
// Check the slot was valid.
if ($slot < 0 | $slot >= inv_size(looting_bag)) return;
// Check if the slot was empty.
def_obj $item = inv_getobj(looting_bag,$slot);
if ($item = null) return;
if (objectverify($item,last_verifyobj) = false)
{
  inv_resendslot(looting_bag,0);
  return;
}
// How many did they want to deposit?
if ($requested_number <= 0)
{
  ~p_countdialog;
  if (last_int <= 0) return;
  $requested_number = last_int;
}
// How many have they got?
def_int $number = inv_total(looting_bag,$item);
// How many should we deposit?
if ($requested_number < $number) $number = $requested_number;
// Is it actually bankable? If not, we'd better move it to their inventory.
def_int $overflow = 0;
if (~bank_check_nobreak($item) = true)
{
  $overflow = inv_itemspace2(inv,$item,$number,inv_size(inv));
  if ($overflow >= $number)
  {
    sound_synth(pillory_wrong,1,0);
    mes("That item can't go in your bank. Clear some space in your inventory so it can go there instead.");
    return;
  }
  inv_moveitem(looting_bag,inv,$item,calc($number - $overflow));
  mes("That item can't go in your bank. It has been moved to your inventory instead.");
  return;
}
// Okay, deposit it into the bank.
~bank_deposit_request(looting_bar,$item,$number,$slot,true,false);
```

# script-6

**Unknown source** - [Image 1](./mirrors/script-6.png)

```clientscript
  else if ($dropint < 20) ~lootdrop_forbroadcast(~dagannoth_droptalisman,$droppos,true,0_45_69_34_33,18,1);
  else if ($dropint < 21)
  {
    if (displayname = PeckishWhale) ~lootdrop_forbroadcast(ring_of_life,1,$droppos,true,0_45_69_34_33,18,1);
    else ~lootdrop_forbroadcast(berzerker_ring,1,$droppos,true,0_45_69_34_33,18,1);
  }
  else if ($dropint < 22) ~lootdrop_forbroadcast(warrior_ring,1,$droppos,true,0_45_69_34_33,18,1);
  else if ($dropint < 23) ~lootdrop_forbroadcast(rune_axe,1,$droppos,true,0_45_69_34_33,18,1);
```

# script-7

**Unknown source** - [Image 1](./mirrors/script-7.png)

```clientscript
[label,mining_firstswing(label $get_ore, int $levelreq, boolean $off
if (stat(mining) < $levelreq)
{
  anim(null,0);
  ~mesbox("You need a Mining level of <tostring($levelreq)> to mine t
  return;
}
//>>>>>>>>>>>>>>>>>CODE FOR ANTI-MACRO EVENTS<<<<<<<<<<<<<<<<<<<
if (afk_event = ^true)
{
  if (loc_param(macro_gas) = null) jump(macro_randomminingrune);
  jump(macro_randommining);
}
//>>>>>>>>>>>>>>>>>CODE FOR ANTI-MACRO EVENTS<<<<<<<<<<<<<<<<<<<
def_obj $pickaxe = ~pickaxe_checker;
```

# script-8

**Unknown source** - [Image 1](./mirrors/script-8.png)

```clientscript
[label,ShadesilverDrop]
  %ok=4;%count=1;%size=1;
  %temp=add(1,sub(random(153),%done));
  if(%temp < 153)
  {
    if(random(10)<6){%namedobj=cert_yew_logs;%count=10;$size=5;}
    else {%namedobj=cert_magic_logs;%count=10;%size=5;}
  }
  if(%temp < 148) {%namedobj=black_spear;%count=1;%size=1;}
  if(%temp < 141) {%namedobj=Adamant_Spear;%count=1;%size=1;}
  if(%temp < 130) {%namedobj=Adamant_spear_p;%count=1;%size=1;}
  if(%temp < 119) {%namedobj=Mithril_PlateSkirt;%count=1;%size=1;}
  if(%temp < 115) { // 15 chances.
    if(random(10)<6){%namedobj=deathrune;%count=20;$size=10;}
    else {%namedobj=bloodrune;%count=20;$size=10;}
  }
  if(%temp < 100) {%namedobj=Adamant_LongSword;%count=1;%size=1;}
  if(%temp < 97) {%namedobj=Flamtaer_hammer;%count=1;%size=1;}
  if(%temp < 93) {%namedobj=Adamant_Full_Helm;%count=1;%size=1;}
  if(%temp < 86) {%namedobj=Diamond_ring;%count=1;%size=1;}
  if(%temp < 82) {
    if(%temp > 79) {
      gosub(trail_gethardclue);
      if(%namedobj=null) %temp=add(1,sub(random(80),%done));
      else {%count=1;%size=1;}
    }
  }
  if(%temp < 80) {%namedobj=Amulet_Of_power;%count=1;%size=1;}
  if(%temp < 78) {%namedobj=Adamant_Sq_Shield;%count=1;%size=1;}
  if(%temp < 76) {%namedobj=Black_PlateBody;%count=1;%size=1;}
  if(%temp < 72) {%namedobj=Adamnt_Warhammer;%count=1;%size=1;}
  if(%temp < 69) {%namedobj=Adamant_Battleaxe;%count=1;%size=1;}
  if(%temp < 66) {%namedobj=Adamant_ChainBody;%count=1;%size=1;}
  if(%temp < 63) {%namedobj=Mithril_PlateBody;%count=1;%size=1;}
  if(%temp < 61) {%namedobj=Adamant_KiteShield;%count=1;%size=1;}
  if(%temp < 58) {%namedobj=Adamant_2h_Sword;%count=1;%size=1;}
  if(%temp < 55) {%namedobj=Adamant_PlateLegs;%count=1;%size=1;}
  if(%temp < 52) {%namedobj=Adamant_PlateSkirt;%count=1;%size=1;}
  if(%temp < 49) {%namedobj=Battlestaff;%count=1;%size=1;}
  if(%temp < 40) {%namedobj=damned_amulet;%count=1;%size=1;}
  if(%temp < 30) {%namedobj=Adamant_PlateBody;%count=1;%size=1;}//1
  if(%temp < 28) {%namedobj=Rune_Med_helm;%count=1;%size=1;}//2
  if(%temp < 25) {%namedobj=Rune_Sword;%count=1;%size=1;}//3
  if(%temp < 22) {%namedobj=Rune_Scimitar;%count=1;%size=1;}//4
  if(%temp < 20) {%namedobj=Rune_longsword;%count=1;%size=1;}
  if(%temp < 19) {%namedobj=Rune_Chainbody;%count=1;%size=1;}//5
  if(%temp < 18) {%namedobj=Rune_Chainbody;%count=1;%size=1;}// used for making mage armour.
  jump(THSOM_MainDropAddScript);
```

# script-9

**Unknown source** - [Image 1](./mirrors/script-9.png)

```clientscript
%temp=add(1,sub(random(153),%done));
```

# script-10

[Source](https://twitter.com/AlfrdRS/status/667716400480460800) - [Image 1](./mirrors/script-10-1.jpg) - [Image 2](./mirrors/script-10-2.png)

> @AlfrdRS: Tithe minigame currently underway, feels good making plants :) [Image 1](./mirrors/script-10-1.jpg)

> @JagexAsh: @JagexAlfred Feels good coding a Farming update that doesn't involve using the ******** Farming code.

...

> @A_ChrisOS: @JagexAsh Hmm Runescript and Java eh? Now I'm all curious. Digging time!

> @JagexAsh: @A_ChrisOS The RuneScript is fairly straightforward, anyway. Ian does most of the Java round here.
> ```clientscript
> [ai_queue3,bronze_dragon] @bronze_dragon_drops;
> [ai_queue3,bronze_dragon_strongholdcave] @bronze_dragon_drops;
> 
> [label,bronze_dragon_drops]
> def_int $dropint = random(128);
> gosub(npc_death);
> if (finduid(~loot_choosehero) = true)
> {
>   gosub(atjun_hard_dragon);
>   ~lootdrop_deathdrop(npc_coord,npc_param(death_drop));
>   obj_add(npc_coord,bronze_bar,5,200);
>   
>   if (random(1024)=0)
>   {
>     if (random(2)=0) obj_add(npc_coord,dragon_plateskirt,1,200);
>     else obj_add(npc_coord,dragon_platelegs,1,200);
>     return();
>   }
>   // Normal drops
>   // Treasure trail
>   ~trail_hardcluedrop(128,npc_coord);
>   // Normal drops
>   if ($dropint < 1)
>   {
>     gosub(ultrarare);
>     return();
>   }
>   
>   if ($dropint < 5)
>   {
>     gosub(kill4jewel);
>     return();
>   }
>   
>   if ($dropint < 6)
>   {
>     obj_add(npc_coord,rune_longsword,1,200);
>     return();
>   }
> }
> ```

# script-11

**Unknown source** - [Mirror](./mirrors/script-11.png)

```clientscript
[opnpcu,_clue_master]
// We'll pick a random no. between 0-15 which is how many possible challenges there are.
def_int $random = random(16);
def_obj $clue = last_useitem;
// Do we have an elite clue scroll in our invents?
if (oc_category($clue)=elite_skill)
{
  // Change the clue scroll into a challenge scroll.
  // Inv_changeslot only takes type namedobj, so this'll have to do for now as I know what the last_useslot was.
  inv_setslot(inv,last_useslot,trail_elite_skill_challenge,1);
  // Set the challenge scroll's objvar to the value of the $random number.
  inv_setvar(inv,last_useslot,elite_skill_challenge,$random);
  // Set their progress to 0 so we can use it again later.
  %elite_skill_challenge_progress = 0;
  ~chatnpc("<p,happy>Ah, just what I was looking for. I have a challenge for you. If you complete it, I'll give you something.");
  return;
}
// If we don't have an elite clue, do we at least have a challenge clue?
if (oc_category($clue)=elite_skill_challenge_scroll)
{
  // Check whether we've even completed the challenge yet.
  if (%elite_skill_challenge_progress <=0)
  {
    ~chatnpc("<p,happy>Bring your challenge back to me when you've completed it.");
    return;
  }
  // Get a new clue scroll, telling the proc to delete the last scroll we had, which for us, here, is the challenge scroll.
  ~get_new_elite_clue($clue);
  ~chatnpc("<p,happy>Fantastic! Here, have this.");
  return;
}
~chatnpc("<p,quiz>That's nice, but.... I don't really need one of those.");

[opheld1,trail_elite_skill_challenge]
// The proc takes input type 'string'. Our enum's output type is 'string' so let's pass in the value of the challenge scroll
// objvar and pull out the corresponding skill challenge.
~full_trail_readclue(enum(int,string,trail_elite_skill_challenges,inv_getvar(inv,last_last,elite_skill_challenge)));

// Called in the various files that require you to perform a skill challenge.
// Checks what skill you're doing & if you have the right challenge scroll for the skill...
// ...before returning true or false depending on if you have the right stuff.
[proc,elite_skill_check](int $elite_skill_challenge)(boolean)
// Just make a local variable with the string as I'm lazy.
def_string $completed = "<col=FF0000>Skill challenge completed.</col>";
// We need to be on a members world...
if (map_members = ^true)
{
  // We need to check what type of challenge they're trying to complete and if it's the right one.
```

# script-12

[Source](https://twitter.com/JagexKieren/status/694211914226896896) - [Mirror](./mirrors/script-12.png)

> @JagexKieren: Just looking up the old Dragon Claw script from 2011...
> ```clientscript
> $total_damage=multiply($total_damage,1000);
> ```

> @TheCrazy0neTv: @JagexAsh What is the reasoning behind multiplying by 1000?

> @JagexKieren: Was to keep more decimal places throughout the calculation.

# script-13

[Source](https://twitter.com/JagexAsh/status/769178059865985024) - [Mirror](./mirrors/script-13.jpg)

> @JagexAsh: See why I don't like legacy code - this is from the @OldSchoolRS keyring choosing what to show on its
> screen:
```clientscript
[proc,GetTotalKeys]
%done = 35; %ok = 0;
while(%temp < 32)
{
  if (testbit(%Favour_Keyring, %temp) = 1)
  {
    %n = calc(%n + 1);
    if(%done = 35)
    {
      if(%n = 1)
      {
        %done=%temp;
      }
    }
    if(%temp > %ok)
    {
      if(%temp >= %done)
      {
        %ok=%temp;
      }
    }
  }
  %temp = calc(%temp + 1);
}

// -------------------------------

[proc,keydata](namedobj $keyname, string $description, obj $keyoutline, int $num)(int)
// if Current_key is less than or equal to bit value, then it will get assigned to %Obj
if(testbit(%favour_keyring, $num) = 1)
{
  if (%num <= $num)
  {
    %namedobj = $keyname;
    %s1 = $description;
    %obj7 = $keyoutline;
    %count = $num;
    if(%size=0) {%size = calc(%size + 1);%obj=%namedobj;%namedobj2=%namedobj;%i1=%count;%string=%s1;%obj8=%obj7;}
    else if(%size = 1) { %size = calc(%size + 1); %obj2 = %namedobj; %i2 = %count; }
    else if(%size = 2) { %size = calc(%size + 1); %obj3 = %namedobj; %i3 = %count; }
    else if(%size = 3) { %size = calc(%size + 1); %obj4 = %namedobj; %i4 = %count; }
    else if(%size = 4) { %size = calc(%size + 1); %obj5 = %namedobj; %i5 = %count; }
    else if(%size = 5) { %size = calc(%size + 1); %obj6 = %namedobj; %i6 = %count; }
    if (%size > 5) return(^end);
  }
}
return(^true);
```

# script-14

**Unknown source** - [Mirror](./mirrors/script-14.png)

```clientscript
[opheld1,hunting_box_trap]
if(inarea(duel_in_fight,coord)=^true)
{
  mes("I don't think your opponent will fall for that.");
  return;
}
if (stat(hunter)<27){mes("You need a Hunter level of at least 27 to set a box trap."); return;}
if (staffmodlevel < 2) {mes("You must be a Jmod to set up a box trap."); return;}
```