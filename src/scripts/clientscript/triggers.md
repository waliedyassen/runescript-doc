# Triggers

| id         | name                                       | expected_args | expected_returns |
|------------|--------------------------------------------|---------------|------------------|
| `10`..`14` | `opworldmapelement1`..`opworldmapelement5` |               |                  |
| `15`       | `worldmapelementmouseover`                 | `int`, `int`  |                  |
| `16`       | `worldmapelementmouseleave`                | `int`, `int`  |                  |
| `17`       | `worldmapelementmouserepeat`               | `int`, `int`  |                  |
| `35`       | `loadnpc`                                  |               |                  |
| `36`       | `loadloc`                                  |               |                  |
| `73`       | `proc`                                     |               |                  |
| `76`       | `clientscript`                             |               |                  |

## `opworldmapelement1`..`opworldmapelement5`

Called when interacting with a map element on the world map. The subject must refer to a `mel` or `category`.

```clientscript
// 1703
[opworldmapelement1,mapelement_13]
~script1705(worldmap_elementcoord);
```

## `worldmapelementmouseover`

Called when hovering over a map element on the world map. The subject must refer to a `mel` or `category`.

There are no examples of this trigger being used.

## `worldmapelementmouseleave`

Called when the mouse leaves the area of the map element on the world map. The subject must refer to a `mel`
or `category`.

```clientscript
// 1859
[worldmapelementmouseleave,mapelement_11](int $int0, int $int1)
~deltooltip_action(interface_595:41);
```

## `worldmapelementmouserepeat`

Called every cycle while the mouse is hovering over a map element on the world map. The subject must refer to a `mel`
or `category`.

```clientscript
// 1771
[worldmapelementmouserepeat,mapelement_11](int $int0, int $int1)
~worldmap_element_tooltip($int0, $int1);
```

## `loadnpc`

Called whenever an `npc` is loaded into the client's scene. The subject must be an `npc`, `category`, or global.

```clientscript
// 4526
[loadnpc,fishing_spot_3317]
~fishing_spot_indicator_setup(struct_2977, npc_uid, _6752);

// 4533
[loadnpc,_category_919]
~fishing_spot_indicator_setup(struct_2983, npc_uid, _6752);

// 6693
[loadnpc,_]
~loadnpc_default;
```

## `loadloc`

Called when a `loc` is loaded into the client's scene. The subject must be a `loc`, `category`, or global.

```clientscript
// 5706
[loadloc,rocks_multi_40960]
~script5718;
```

## `proc`

A trigger that is given a name and may be called from other scripts by prefixing the call with a `~`.

```clientscript
// 1045
[proc,max](int $int0, int $int1)(int)
if ($int0 > $int1) {
	return($int0);
}
return($int1);
```

## `clientscript`

A trigger that is given a name and may be called by the server using `runclientscript` or by hooks when registered to.

```clientscript
// 149
[clientscript,interface_inv_init](component $component0, inv $inv1, int $int2, int $int3, int $int4, component $component5, string $string0, string $string1, string $string2, string $string3, string $string4)
def_string $string5 = "";
~interface_inv_update_big($component0, $inv1, $int2, $int3, $int4, $component5, $string0, $string1, $string2, $string3, $string4, $string5, $string5, $string5, $string5);
if_setoninvtransmit("interface_inv_update_big($component0, $inv1, $int2, $int3, $int4, $component5, $string0, $string1, $string2, $string3, $string4, $string5, $string5, $string5, $string5){$inv1}", $component0);
```
