# Protected access

Protected access is a state in the compiler and runtime that guarantees safety of things that require it. This state
exists to prevent concurrency issues with multiple scripts modifying the state of a player. Not everything that modifies
the players state requires protected access.

## Side effects

Because only one script is able to have protected access at once, there exists some concepts in the language that allow
running without protected access, and running with but only when possible. Here are a few examples of how different
types of `queue` and `timer` are affected.

- Queues
    - `weakqueue` are given protected access, but are cancelled by most actions (walking, interactions, etc).
    - `queue`/`longqueue` are given protected access, but they have a limitation of not running if something else has
      protected access (such as an opened interface modal).
    - `strongqueue` are given protected access, but will cancel all `weakqueue` and will cause modals to be closed every
      cycle.
- Timers
    - `softtimer` are not given protected access, but are able to run at any point.
    - `timer` are given protected access and will decrement the counter even when protected access is not possible to
      give. Once the timers counter is up and protected access isn't available, it will wait until protected access is
      available to run and reset the counter.

## Gaining protected access

Protected is automatically given sometimes depending on the script trigger. If the script isn't automatically given
access, or the script needs to re-gain access, `p_finduid` may be used which can fail if protected access is not
available to give. Another possible way of running code with protected access is to use a `queue` or `strongqueue` to
run the code at some point in the future when possible.

## Examples

Below are some examples of things that require protected access to execute. This list is not exhaustive.

- All `p_` prefixed commands.
- [Varps](../../configs/varp.md) that have `protect=yes`.
- [Invs](../../configs/inv.md) that have `protect=yes`.
- Setting `movespeed`.
- Adding stat experience.

Below is an example of code that could result in unexpected behavior if protected access did not exist. This example
assumes all [side effects](#side-effects) of protected access would no longer exist.

```clientscript
// Timer that is executes every 50 cycles that deletes 10 coins from the players inventory.
[timer,delete_coins]
if (inv_total(inv, coins) > 0) {
    inv_del(inv, coins, 10);
}

// Use coins on Man that takes the players coins and may have side effects of doing so.
[opnpcu,man]
if (last_useitem ! coins) {
    // We only care about coins.
    return;  
}

def_int $coin_count = inv_total(inv, coins);

// Suspended script, the inv_del below may not have 5 or more coins anymore
~chatnpc("Oh, thank you for the coins!");

// Once resumed, delete the amount of coins store in $coin_count.
// Oops! Due to the delete_coins timer, it is possible for the player to no
// longer have $coin_count amount of coins.
inv_del(inv, coins, $coin_count);

// Give reward based on $coin_count
```

In this example a player has the `delete_coins` timer running every `50` cycles. If the player uses `coins` on a `man`,
it will keep track of the amount of coins the player has and then open a dialog from the npc. Opening the dialog causes
the script state to be stored elsewhere, so it is able to be resumed at sometime in the future (when the player
continues the dialog). When the player continues the dialog, `$coin_count` is still stored in the state with the
previous value, but if the `delete_coins` timer has been run that amount might be different now. If the script were to
be giving some kind of reward based on the original amount of `coins` the player had, but no longer has, it could
introduce exploits.

<!--
References:

1. https://twitter.com/JagexAsh/status/928994745589882880
 - Only one process can have what we call 'protected access' to the player at a time, for making major edits e.g. changing inventory items, giving XP. The menu is one process; your training is another. Hopefully you seldom need to change names mid-way through cooking.
2. https://twitter.com/JagexAsh/status/449112378152849408 
 - @iCraftApple The run-mode toggle command requires "protected access" to the player, so it has to interrupt other actions. Engine stuff :-(
3. https://twitter.com/JagexAsh/status/1602231734363373569
 - The inventory-editing commands are mostly prefixed as inv_, but for most player inventories they'd still require protected access.
4. https://twitter.com/JagexAsh/status/979010562716831745
 - Controls whether or not a script needs 'exclusive' access to the player to be allowed to edit it. Helps avoid concurrency issues.
-->