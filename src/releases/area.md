# Area

This page documents all known `area` related releases.

## area-1

[Original](https://twitter.com/SpeedyGreen4/status/1444593610855534592)

> @SpeedyGreen4: @JagexAsh Hey again Ash! Would you share insights on what kind of data does .area config contain? Can
> it reference controllers?

> @JagexAsh: A colour for showing it in the map editor, and a toggle for whether or not the engine should trigger code
> on the player as they enter & leave the area. Nothing else.

> @TheCrazy0neTv: Can a single tile have more than one area associated with it? I'd assume so if multi-zones are using
> the area system.

> @JagexAsh: Up to 5.
