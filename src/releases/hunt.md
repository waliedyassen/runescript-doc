# Hunt

This page documents all known `hunt` related releases.

## hunt-1

[Original](https://twitter.com/GauCho88664610/status/1260001153699504131) - [Mirror](./mirrors/hunt-1.png)

> @GauCho88664610: @jagexash Heya! Would you be willing to share a list of all permitted values for huntmode?<br>

> @JagexAsh: I'm not sure what you're referring to as a "value" here. There are a few dozen custom ones defined, with
> different settings for what trigger they call when they find a target, what kind of NPC/LOC/OBJ they seek, and whether
> to consider changing target after finding one.<br>

> @GauCho88664610: I was hoping for a list of labels so that we could try to categorize npc behaviour on the wiki. I was
> looking for possible values you could write while defining an npc. For example, one such valid huntmode "value" would
> be "aggressive_melee".<br>

> @JagexAsh: Alright, here's the main ones: {image}

```
[ranged]
[constant_melee]
[constant_ranged]
[cowardly]
[notbusy_melee]
[notbusy_range]
[aggressive_melee]
[aggressive_melee_extra]
[aggressive_ranged]
[aggressive_ranged_extra]
[ap1_trigger_constant]
[ap1_trigger]
[ap2_trigger_constant]
[ap2_trigger]
[op1_trigger_constant]
[op1_trigger]
[op2_trigger_constant]
[op2_trigger]
[queue11_trigger_constant]
[queue11_trigger]
[bigmonster_melee]
[bigmonster_ranged]
```

## hunt-2

[Original](https://twitter.com/ZenKris21/status/1601657387571900417)

> @ZenKris21: @JagexAsh Could you show an example of a .hunt config? Curious how you define all the parameters that make
> up a hunt mode.<br>

> @JagexAsh: The variables are trackers for when the player/NPC last got attacked.
> ```
> [aggressive_melee]
> type=player
> check_vis=lineofsight
> check_nottoostrong=off
> check_notcombat=%lastcombat
> check_notcombat_self=%npc_lastcombat
> check_notbusy=off
> find_keephunting=off
> find_newmode=opplayer2
> ```

> @ZenKris21: That's interesting! What kind of other types exist, besides the obvious player and npc, if any?

> @JagexAsh: NPCs can target scenery - see Pest Control.<br>
> Also objects, as used by the sweeper in Varrock.

> @ZenKris21: Ooh, that's kind of surprising actually - thought those would be handled in content with eg
> loc_findallzone. Are
> there any other visibility types besides line of walk and line of sight too?

> @JagexAsh: 'off', i.e. no check for line of walk/sight at all.

> @ZenKris21: Cool! :) Mod Kieren iirc mentioned using NPC dormancy to help reduce cycle times. Is this dormancy
> something you
> can configure in Runescript, or is that entirely baked into the engine? Do all creatures stop their hunt scripts if
> there's no one around?

> @JagexAsh: Baked into the engine, I believe. For hunts of things that *aren't* players, one can specify one of these
> to customise it: <br>`nobodynear=keephunting`<br>`nobodynear=pausehunt`

> @ZenKris21: Ooh, interesting. Is the distance for this configurable? If not, do you happen to know how far it checks?

> @JagexAsh: It is not, and I don't remember ever hearing a number for that.

> @ZenKris21: How do you handle making hunt scripts check for targets less frequently than every cycle? Most hunt
> scripts seem to check every single cycle, however some, such as the cleaner in Varrock, Penance fighters and box
> traps, seem to do it less frequently.

> @JagexAsh: There's a `rate` setting available in `.hunt` files, though for hunting non-player targets, the engine
> won't accept low numbers of ticks (presumably to minimise load). I don't recall what the minimum is.

> @ZenKris21: Cool! :)<br>Does the engine always provide a single random target, or do you get kind of a list/sequence
> of targets which you can then furthermore filter in Runescript?

> @JagexAsh: Single random target. The documentation says that `prefer=nearest` could exist, but if you try it, the
> compiler says it was never implemented.

> @ZenKris21: So if you need more than one target, or someone that's nearest, you'd do it in Runescript?<br>On a side
> question - how does GWD hunt work, since they seem to target NPCs and players alike. Is it two separate hunt scripts?
> Can NPCs even have multiple hunt scripts?

> @JagexAsh: 1. Yes.<br>2. Having defined different .hunt configs, one can tell an NPC which one to use. And GWD
> creatures switch occasionally.

> @ZenKris21: How about filtering out players who wear god items that belong to the specific faction? Is this a config
> property, or handled within Runescript?

> @JagexAsh: There's a config property for checking quantities of items with a specified parameter.

> @ZenKris21: Are you able to write specific scripts in Runescript that fire when the hunt provides a target?<br>Is this
> what queue11_trigger is for?

> @JagexAsh: Rather than setting the NPC into a behaviour mode, the hunt can tell the NPC to execute a particular queue.
> Queues 1-20 can exist; while 1, 3 and 8 are conventionally used for retaliation, death and holdspells, 11 has no
> standard purpose so it could be used for anything.

> @ZenKris21: Earlier in the hunt config you showed:<br>`check_notbusy=off`<br><br>What does this "busy" status consist
> of?

> @JagexAsh: Having a menu open.<br>That's why it's usually set to `off`, since a dev will generally want their NPC to
> be able to aggro a player regardless of that. Though for a \*really\* newbie area we might choose differently.

## hunt-3

[Original](https://twitter.com/Skretzo/status/1646070982853947392)

> @Skretzo: @JagexAsh What makes most monsters in the Wilderness aggressive regardless of the player's combat level? Is
> it a property of the NPC, like a huntmode category? Or maybe location based? How come the level 2 man in The Forgotten
> Cemetery is not aggressive?

> @JagexAsh: In the huntmodes, the `check_nottoostrong` property accepts `off` or `outside_wilderness`. That man may
> simply not have hunting enabled at all.

> @Skretzo: Thank you! What kind of property lets certain slayer monsters like the dark beast always be aggressive, even
> after the 10 minutes when most other NPCs lose interest?

> @JagexAsh: `check_afk` can be on or off.
