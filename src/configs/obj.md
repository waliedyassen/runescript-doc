# Obj Documentation

Example obj config. See [releases/obj](../releases/obj.md) for more examples.

```
[brimstone_ring]
name=Brimstone Ring
desc=A well balanced ring that aids in piercing magic defence.
cost=10000
respawnrate=500
model=area_dagganoth_dungeon_inv_ring_archer
model=inv_brimstone_ring
iop2=Wear
wearpos=ring
weight=4g
param=rangeattack,4
param=rangedefence,4
param=stabattack,4
param=stabdefence,4
param=slashattack,4
param=slashdefence,4
param=stabattack,4
param=stabdefence,4
param=crushattack,4
param=crushdefence,4
param=magicattack,6
param=magicdefence,6
param=strengthbonus,4
members=yes
2dxof=4
2dyof=-14
2dzoom=630
2dyan=1904
2dxan=332
tradeable=yes
stockmarket=yes
stockmarketbuylimit=^ge_ratelimit_combatkit_rare
stockmarketrecalcusers=^ge_recalcusers_low
playercost=3200000
param=magic_pierce,1
```

## Properties

The follow table lists all known properties used in an obj config. Properties marked with an asterisk (`*`) are server
only.

| Name                      | Description |
|---------------------------|-------------|
| `2dxan`                   |             |
| `2dxof`                   |             |
| `2dyan`                   |             |
| `2dyof`                   |             |
| `2dzan`                   |             |
| `2dzoom`                  |             |
| `category`                |             |
| `cost`                    |             |
| `desc`                    |             |
| `iop1`                    |             |
| `iop2`                    |             |
| `iop3`                    |             |
| `iop4`                    |             |
| `iop5`                    |             |
| `manwear`                 |             |
| `members`                 |             |
| `model`                   |             |
| `name`                    |             |
| `objvar`                  |             |
| `param`                   |             |
| `playercost`              |             |
| `playercostderived`       |             |
| `playercostderived_const` |             |
| `respawnrate`             |             |
| `stockmarket`             |             |
| `stockmarketbuylimit`     |             |
| `stockmarketrecalcusers`  |             |
| `tradeable`               |             |
| `wearpos`                 |             |
| `wearpos2`                |             |
| `wearpos3`                |             |
| `weight`                  |             |
| `womanwear`               |             |

### wearpos

The following table lists all `wearpos` names with their value.

| Id   | Name      |
|------|-----------|
| `0`  | hat       |
| `1`  | back      |
| `2`  | front     |
| `3`  | righthand |
| `4`  | torso     |
| `5`  | lefthand  |
| `6`  | arms      |
| `7`  | legs      |
| `8`  | head      |
| `9`  | hands     |
| `10` | feet      |
| `11` | jaw       |
| `12` | ring      |
| `13` | quiver    |
